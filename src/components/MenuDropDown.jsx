import React from 'react';

const MenuDropDown = () => {
    return (
        <div className="dropdown">
            <button className="btn btn-danger dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-bs-toggle="dropdown">
                Weekly Menu / Service Plan
            </button>
            <div className="dropdown-menu" aria-labelledby="dropdownMenu2">
                <button className="dropdown-item" type="button">Mon - Thu Dinner Menu</button>
                <button className="dropdown-item" type="button">Friday Dinner Menu</button>
                <button className="dropdown-item" type="button">Sat & Sund Lunch Menu</button>
                <button className="dropdown-item" type="button">Sat & Sund Dinner Menu</button>
            </div>
        </div>
    )
}

export default MenuDropDown;